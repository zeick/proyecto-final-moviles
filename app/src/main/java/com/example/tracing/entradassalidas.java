package com.example.tracing;

public class entradassalidas {
    private String nombre;
    private String fecha;
    private String horaEntrada;
    private String horaSalida;

    public entradassalidas(String nombre, String fecha, String horaEntrada, String horaSalida) {
        this.nombre = nombre;
        this.fecha = fecha;
        this.horaEntrada = horaEntrada;
        this.horaSalida = horaSalida;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getFecha() {
        return fecha;
    }
    public String getHoraEntrada() {
        return horaEntrada;
    }
    public String getHoraSalida() {
        return horaSalida;
    }

    public entradassalidas(){

    }
    public String toString(){
        return this.nombre + ", " + this.fecha + ", " + this.horaEntrada + ", " + this.horaSalida;
    }
}
