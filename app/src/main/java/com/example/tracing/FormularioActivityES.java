package com.example.tracing;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

import static com.example.tracing.App.CHANNEL_1_ID;

public class FormularioActivityES extends AppCompatActivity {
    DatabaseReference mRootReference;
    Button mButtonSubirDatosFirebase;
    EditText mEditTextDatoNombreVisitante, mEditTextDatoMotivoVisitante, mEditTextDatoNumeroResidencia,
            mEditTextDatoHoraEntradaVisitante, mEditTextDatoHoraSalidaVisitante, mEditTextDatoFechaVisitante;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_e_s);
        final Intent cinemaIntent = new Intent(this, MainActivity.class);


        //Database
        mRootReference = FirebaseDatabase.getInstance().getReference();

        mButtonSubirDatosFirebase = findViewById(R.id.btn_enviar);

        mEditTextDatoNombreVisitante = findViewById(R.id.etnombre);
        mEditTextDatoHoraEntradaVisitante = findViewById(R.id.ethoraEntrada);
        mEditTextDatoHoraSalidaVisitante = findViewById(R.id.ethoraSalida);
        mEditTextDatoFechaVisitante = findViewById(R.id.etfecha);




        mButtonSubirDatosFirebase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre = mEditTextDatoNombreVisitante.getText().toString();
                String horaEntrada = mEditTextDatoHoraEntradaVisitante.getText().toString();
                String horaSalida = mEditTextDatoHoraSalidaVisitante.getText().toString();
                String fecha = mEditTextDatoFechaVisitante.getText().toString();

                Map<String, Object> datosVisitantes = new HashMap<>();
                datosVisitantes.put("nombre", nombre);
                datosVisitantes.put("horaEntrada", horaEntrada);
                datosVisitantes.put("horaSalida", horaSalida);
                datosVisitantes.put("fecha", fecha);

                mRootReference.child("Entradas Salidas").push().setValue(datosVisitantes);
                Toast.makeText(getApplicationContext(), "Se ha enviado la solicitud. ", Toast.LENGTH_SHORT).show();
                startActivity(cinemaIntent);
                //sendOnChannel1(v);
            }
        });
//Database
    }


}