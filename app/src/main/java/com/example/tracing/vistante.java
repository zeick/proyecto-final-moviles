package com.example.tracing;

public class vistante {
    private String nombre;
    private String fecha;
    private String horaEntrada;
    private String horaSalida;
    private String motivo;
    private String numResidencia;

    public vistante(String nombre, String fecha, String horaEntrada, String horaSalida, String motivo, String numResidencia) {
        this.nombre = nombre;
        this.fecha = fecha;
        this.horaEntrada = horaEntrada;
        this.horaSalida = horaSalida;
        this.motivo = motivo;
        this.numResidencia = numResidencia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getFecha() {
        return fecha;
    }
    public String getHoraEntrada() {
        return horaEntrada;
    }
    public String getHoraSalida() {
        return horaSalida;
    }
    public String getMotivo() {
        return motivo;
    }
    public String getNumResidencia() {
        return numResidencia;
    }

    public vistante(){

    }
    public String toString(){
        return this.nombre + ", " + this.fecha + ", " + this.motivo + ", " + this.numResidencia + ", " + this.horaEntrada + ", " + this.horaSalida;
    }
}
