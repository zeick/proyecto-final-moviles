package com.example.tracing;

import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback {
    Handler handler = new Handler();
    private GoogleMap mMap;
    double lat=21.8519763333, lon=-102.2648795;

    String _lataux="0", _lonaux="0";
    //String _lataux="21.8519763333", _lonaux="-102.2648795";
    String _lat="21.8585", _lon="21.8585";
    MarkerOptions a = new MarkerOptions().position(new LatLng(lat,lon)).title("Visitante");
    private int TIEMPO = 5000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.addMarker(a);

        ejecutarTarea();
        // Add a marker in Sydney and move the camera





    }
    public String shell_exec(String cmd)
    {
        String o=null;
        try
        {
            Process p=Runtime.getRuntime().exec(cmd);
            BufferedReader b=new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            while((line=b.readLine())!=null)o+=line;
        }catch(Exception e){o="error";}
        return o;
    }


    public void ejecutarTarea() {


        handler.postDelayed(new Runnable() {

            public void run() {

                // función a ejecutar

                actualizarChofer(); // función para refrescar la ubicación del conductor, creada en otra línea de código

                handler.postDelayed(this, TIEMPO);
            }

        }, TIEMPO);

    }
    @Override
    public void onBackPressed() {
            TIEMPO=100000;
            super.onBackPressed();

    }
    public void  actualizarChofer(){
        _lat = shell_exec("curl 192.168.0.200/latitud.html");//.replace("null", "");
        _lon = shell_exec("curl 192.168.0.200/longitud.html");//.replace("null", "");
        if(_lat==null&&_lon==null){
            Toast.makeText(getApplicationContext(), "No se detecta GPS", Toast.LENGTH_SHORT).show();
        }
        if(_lat==null) {
            //Toast.makeText(getApplicationContext(), "nulo lat ", Toast.LENGTH_SHORT).show();
            _lat=_lataux;
        }else{
            _lat=_lat.replace("null", "");
            _lataux=_lat;
        }
        if(_lon==null) {
            //Toast.makeText(getApplicationContext(), "nulo lon", Toast.LENGTH_SHORT).show();
            _lon=_lonaux;
        }else{
            _lon=_lon.replace("null", "");
            _lonaux=_lon;
        }
        //Toast.makeText(getApplicationContext(), _lat + ", " + _lon, Toast.LENGTH_SHORT).show();
        //}while (_lat.length() < 5 || _lon.length() < 5);

        this.lat = Double.parseDouble(_lat);
        this.lon = Double.parseDouble(_lon);


        LatLng sydney = new LatLng(lat, lon);
        float zoom = 20;
        a.position(new LatLng(lat,lon));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney,zoom));
        mMap.getUiSettings().setZoomControlsEnabled(true);

    }
}