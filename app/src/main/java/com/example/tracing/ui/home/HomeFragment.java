package com.example.tracing.ui.home;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.tracing.R;
import com.example.tracing.entradassalidas;
import com.example.tracing.vistante;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HomeFragment extends Fragment {

    private DatabaseReference databaseReference;
    private DatabaseReference databaseReference2;
    private HomeViewModel homeViewModel;
    ListView listView;
    ListView listView2;
    ArrayList<String> arrayList = new ArrayList<>();
    ArrayAdapter<String> arrayAdapter;
    ArrayList<String> arrayList2 = new ArrayList<>();
    ArrayAdapter<String> arrayAdapter2;
    Button es, v;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        databaseReference=FirebaseDatabase.getInstance().getReference("Visitante");
        listView = root.findViewById(R.id.lista_vistantes);
        //Toast.makeText(getContext(), "nulo lat ", Toast.LENGTH_SHORT).show();
        arrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, arrayList);
        listView.setAdapter(arrayAdapter);
        databaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                String value = snapshot.getValue(vistante.class).toString();
                arrayList.add(value);
                arrayAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        databaseReference2= FirebaseDatabase.getInstance().getReference("Entradas Salidas");
        listView2 = root.findViewById(R.id.lista_entradassalidas);
        //Toast.makeText(getContext(), "nulo lat ", Toast.LENGTH_SHORT).show();
        arrayAdapter2 = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, arrayList2);
        listView2.setAdapter(arrayAdapter2);
        databaseReference2.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                String value = snapshot.getValue(entradassalidas.class).toString();
                arrayList2.add(value);
                arrayAdapter2.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


        v = root.findViewById(R.id.button);
        es = root.findViewById(R.id.button2);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listView.setVisibility(View.VISIBLE);
                listView2.setVisibility(View.GONE);
            }
        });

        es.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listView.setVisibility(View.GONE);
                listView2.setVisibility(View.VISIBLE);
            }
        });

/*
        mRootReference = FirebaseDatabase.getInstance().getReference();
        mRootReference.child("Visitante").addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren() ){

                    Log.e("Datos:", ""+snapshot.getValue());




                }




            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
*/











        return root;
    }
}