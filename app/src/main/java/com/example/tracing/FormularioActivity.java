package com.example.tracing;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

import static com.example.tracing.App.CHANNEL_1_ID;
import static com.example.tracing.App.CHANNEL_2_ID;

public class FormularioActivity extends AppCompatActivity {
    private NotificationManagerCompat notificationManager;
    DatabaseReference mRootReference;
    Button mButtonSubirDatosFirebase;
    EditText mEditTextDatoNombreVisitante, mEditTextDatoMotivoVisitante, mEditTextDatoNumeroResidencia,
            mEditTextDatoHoraEntradaVisitante, mEditTextDatoHoraSalidaVisitante, mEditTextDatoFechaVisitante;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario);
        notificationManager = NotificationManagerCompat.from(this);
        final Intent cinemaIntent = new Intent(this, MainActivity.class);


        //Database
        mRootReference = FirebaseDatabase.getInstance().getReference();

        mButtonSubirDatosFirebase = findViewById(R.id.btn_enviar);

        mEditTextDatoNombreVisitante = findViewById(R.id.etnombre);
        mEditTextDatoMotivoVisitante = findViewById(R.id.etmotivo);
        mEditTextDatoNumeroResidencia = findViewById(R.id.etNumeroResidencia);
        mEditTextDatoHoraEntradaVisitante = findViewById(R.id.ethoraEntrada);
        mEditTextDatoHoraSalidaVisitante = findViewById(R.id.ethoraSalida);
        mEditTextDatoFechaVisitante = findViewById(R.id.etfecha);




        mButtonSubirDatosFirebase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre = mEditTextDatoNombreVisitante.getText().toString();
                String motivo = mEditTextDatoMotivoVisitante.getText().toString();
                String numResidencia = mEditTextDatoNumeroResidencia.getText().toString();
                String horaEntrada = mEditTextDatoHoraEntradaVisitante.getText().toString();
                String horaSalida = mEditTextDatoHoraSalidaVisitante.getText().toString();
                String fecha = mEditTextDatoFechaVisitante.getText().toString();

                Map<String, Object> datosVisitantes = new HashMap<>();
                datosVisitantes.put("nombre", nombre);
                datosVisitantes.put("motivo", motivo);
                datosVisitantes.put("numResidencia", numResidencia);
                datosVisitantes.put("horaEntrada", horaEntrada);
                datosVisitantes.put("horaSalida", horaSalida);
                datosVisitantes.put("fecha", fecha);

                mRootReference.child("Visitante").push().setValue(datosVisitantes);
                Toast.makeText(getApplicationContext(), "Se ha enviado la solicitud. ", Toast.LENGTH_SHORT).show();
                startActivity(cinemaIntent);
                sendOnChannel1(v);
            }
        });
//Database
    }
    public void sendOnChannel1(View v) {
        String title = "Nueva visita";
        String message = mEditTextDatoNombreVisitante.getText().toString()+" desea visitarte.";

        Intent activityIntent = new Intent(this, MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this,
                0, activityIntent, 0);

        Intent broadcastIntent = new Intent(this, NotificationReceiver.class);
        broadcastIntent.putExtra("toastMessage", "Aceptado");
        PendingIntent actionIntent = PendingIntent.getBroadcast(this,
                0, broadcastIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent broadcastIntent2 = new Intent(this, NotificationReceiver2.class);
        broadcastIntent2.putExtra("toastMessage", "En puerta");
        PendingIntent actionIntent2 = PendingIntent.getBroadcast(this,
                0, broadcastIntent2, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent broadcastIntent3 = new Intent(this, NotificationReceiver3.class);
        broadcastIntent3.putExtra("toastMessage", "Rechazado");
        PendingIntent actionIntent3 = PendingIntent.getBroadcast(this,
                0, broadcastIntent3, PendingIntent.FLAG_UPDATE_CURRENT);



        Notification notification = new NotificationCompat.Builder(this, CHANNEL_1_ID)
                .setSmallIcon(R.mipmap.ic_icono_round)
                .setContentTitle(title)
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setColor(Color.BLUE)
                .setContentIntent(contentIntent)
                .setAutoCancel(true)
                .setOnlyAlertOnce(true)
                .addAction(R.mipmap.ic_launcher, "Aceptar", actionIntent)
                .addAction(R.mipmap.ic_launcher, "En puerta", actionIntent2)
                .addAction(R.mipmap.ic_launcher, "Rechazar", actionIntent3)
                .build();
        notificationManager.notify(1, notification);
    }

}