package com.example.tracing;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;

public class NotificationReceiver extends BroadcastReceiver {
    @Override

    public void onReceive(Context context, Intent intent) {
        String message = intent.getStringExtra("toastMessage");
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        //final Intent cinemaIntent = new Intent(context, MainActivity.class);
        //context.startActivity(cinemaIntent);

    }

}